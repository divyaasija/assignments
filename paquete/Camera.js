import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  Image,
  Text,
} from 'react-native';
import Actions from 'react-native-router-flux';
import Camera from 'react-native-camera'



export default class CameraView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Imagepath: null,
    };
  }

  clickPicture() {
    this.camera.capture()
      .then((data) => {
        this.setState({ Iamgepath: data.Imagepath })
      })
      .catch(err => console.error(err));
  }
  

  showCamera() {
    return (
      <View>
      <Camera
        ref={(cam) => {
          this.camera = cam;
        }}
        style={styles.preview}
        aspect={Camera.constants.Aspect.fill}
        captureTarget={Camera.constants.CaptureTarget.camerRoll}
      >
        <TouchableHighlight
          style={styles.captureButton}
          onPress={()=>this.clickPicture()}
          underlayColor="rgba(255, 255, 255, 0.5)"
        >
          <View />
        </TouchableHighlight>
        
      </Camera>
      </View>
    );
  }

  showImage() {
    return (
      <View>
        <Image
          source={{ uri: this.state.Imagepath }}
          style={styles.preview}
        />
        <Text
          style={styles.cancelOption}
          onPress={() => this.setState({ Imagepath: null })}
        >Cancel
        </Text>
        {/* <Text
          style={styles.ok}
          onPress={()=>Actions.CameraRoll()}
        >OK
        </Text> */}
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.Imagepath ? this.showImage() : this.showCamera()}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  captureButton: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: '#FFF',
    marginBottom: 15,
  },
  cancelOption: {
    position: 'absolute',
    right: 20,
    top: 20,
    backgroundColor: 'transparent',
    color: '#FFF',
    fontWeight: '600',
    fontSize: 17,
  },

});
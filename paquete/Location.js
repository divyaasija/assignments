import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Button, 
} from 'react-native';
import MapView  from 'react-native-maps';
import Geocoder from 'react-native-geocoding';

export default class Location extends Component  {
    constructor(){
        super();
        this.state={
            userLocation:null
        }
    }
 
    getLocation=()=>{
        navigator.geolocation.getCurrentPosition(position=>{
            this.setState({
                userLocation:{
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude,
                    latitudeDelta: 0.0622,
                    longitudeDelta: 0.421
                }
            })
        },err=>alert(err))
    };
   
    render(){
        let locationMarker=null
        if(this.state.userLocation){
           locationMarker=<MapView.Marker coordinate={this.state.userLocation}/>
        }
        
        return(
            <View style={styles.container} >
           
            <MapView style={styles.map}
             initialRegion={{
                latitude: 20.5937,
                longitude:  78.9629,
                latitudeDelta: 0.0622,
                longitudeDelta: 0.0421,
              }}
              region={this.state.userLocation}
            >
            {locationMarker}
                
            </MapView>
            
              
              <Button title="get location" onPress={ this.getLocation}/>
            </View>
                
          
        )
    }
}

const styles = StyleSheet.create({
    container:{
       width:'100%',
       height:450
    },
    map:{
      height:'100%',
      width:'100%'
        
  
    }
})
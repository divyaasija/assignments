import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ShopListDetails from './ShopListDetails'
import ShopView from './ShopView'
import { Router, Scene,ActionConst } from 'react-native-router-flux';


export default class App extends React.Component {
  render() {
    return (
      <Router>
      <Scene key="root">

        <Scene 
          key="ShopListDetails"
          component={ShopListDetails}
          title="Shop List Details"
        />
        
        <Scene 
          key="ShopView"
          component={ShopView}
          title="ShopView"
          initial
        />
        
      </Scene>
      </Router>
    );
  }
}
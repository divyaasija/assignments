import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux'; 

export default class Register extends React.Component {
  constructor(){
    super();
    this.state={
      registerDetails:{
        uname:'',
        password:'' 
      }
  }
  }
  
  storeRegistrationDetails=async ()=>{
    const username=this.state.registerDetails.uname
    const password=this.state.registerDetails.password;
    let users = [];
    let existUsers = AsyncStorage.getItem('users');
    if(existUsers == null){
      users.push(JSON.stringify(this.state.registerDetails));
    }
    else{
      users = existUsers;
      users.push(this.state.registerDetails);
    }
    AsyncStorage.setItem('users', JSON.stringify(users));
     alert("Registered Successfully")
      Actions.login({registeredUserName:this.state.registerDetails.uname,registeredPassword:this.state.registerDetails.password});
    }
  

  render() {
    return (
      <View style={styles.container}>
      <Image style={styles.logo}
          source={require('./images/logo.png')}
        />
     
      <View style={styles.inputBox}>
      
      <TextInput  placeholder="Username" 
         onChangeText={uname =>
          this.setState({
            registerDetails: {
              ...this.state.registerDetails,
              uname: uname
            }
          })}
         />
        <TextInput 
        secureTextEntry={true}
         placeholder="password" 
         onChangeText={password =>
          this.setState({
            registerDetails: {
              ...this.state.registerDetails,
              password: password
            }
          })
        }/>
      <Button
          style={styles.button}
          onPress={()=>this.storeRegistrationDetails()}
          title="Register"
          />
      </View>
      
    </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    height:155,
    width:150,
},


  inputBox:{
      width:300,
      marginVertical:25, 
  },
  button:{
    color:"#3ADF00"
  }
 
});

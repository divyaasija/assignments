import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux'; 
export default class Login extends React.Component {
    constructor(){
        super();
        this.state={
          loginDetails:{
            uname:'',
            password:''
          }
        }
    }
   
    
    async loginHandler(){
      let context = this;
      let user=this.state.loginDetails.uname
      try {
         let value =  AsyncStorage.getItem(user);
         if (value != null){
             Actions.Dashboard();
            alert('logged in')
         }
         else {
           alert("not logged in")
        }
      } catch (error) {
        
        alert(error)
      }
  }
    
    
  render() {
    return (
      <View style={styles.container}>
      <Image style={styles.logo}
          source={require('./images/logo.png')}
        />
         
        <View style={styles.inputBox}>
        
        <TextInput  placeholder="Username" 
         onChangeText={uname =>
          this.setState({
            loginDetails: {
              ...this.state.loginDetails,
              uname: uname
            }
          })}
         />
        <TextInput 
        secureTextEntry={true}
         placeholder="password" 
         onChangeText={password =>
          this.setState({
            loginDetails: {
              ...this.state.loginDetails,
              password: password
            }
          })
        }/>
        <Button
            style={styles.button}
            onPress={()=>this.loginHandler()}
            title="Login"
            />
        </View>
        <TouchableOpacity onPress={() => Actions.register()}>
        <Text>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    height:155,
    width:150,
},
  
  inputBox:{
      width:300,
      marginVertical:25, 
  },
  button:{
    color:"#3ADF00"
  }
 
});



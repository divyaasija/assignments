import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet,Image ,ScrollView} from 'react-native'
import { StackNavigator } from 'react-navigation';

class List extends Component {
  static navigationOptions =
  {
     title: 'ITEM LIST',
  };
   state = {
      names: [
         {
            id: 0,
            name:"My FIRST STORY BOOK",
            pages:"Approximately 20 page",
  published:"Hardcover – October 22, 2013",
  author:"Alisa Solomon",
            image:require("./image/book1.jpg"),
            description:"My Very Own® Name is made especially for each child, featuring the child’s first name, last name and birth date. In this beautifully illustrated story, animal characters bring letters one by one to spell the first and last names of the child.The tiger brings the T, the yak brings the Y, and so on, until the child’s first and last names are spelled in rhyme. At the end, the animals celebrate because they’ve created the perfect name. The book also includes an illustrated encyclopedia of 61 animals, with fun facts about each."
         },
         {
            id: 1,
            name: 'SEA OF LOVE (Simon Romantic Comedies)',
            pages:"Paperback, 236 pages",
            published:"Published December 16th 2008 by Simon Pulse",
            author:"by Simon Pulse",
            image:require("./image/book2.jpg"),
            description:"Sea of Love is a 1989 American neo-noir thriller film directed by Harold Becker, written by Richard Price and starring Al Pacino, Ellen Barkin and John Goodman. The story concerns a New York City detective trying to catch a serial killer who finds victims through the singles column in a newspaper."
         },
         {
            id: 2,
            name: 'THE ARABIAN NIGHTS',
            pages:"Wafa’ Tarnowska ",
            published:"Barefoot Books (December 2010)",
            author:"Carole Hénaff (Illustrator)",
            image:require("./image/book3.jpg"),
            description:"n this book, author and translator Wafa’ Tarnowska retells the stories of The Thousand and One Nights, or The Arabian Nights in a large, generously illustrated volume that is meant to be a permanent part of a home or library collection. And rightly so, because this ancient collection of tales and fables from Persia, India, and the Arab world has, as Tarnowska writes in her foreword, “become a part of the world’s cultural heritage.” In them, the bride Shahrazade saves her own life, and that of countless other young women, by telling tales for one thousand and one nights to her new husband, the king Shahriyar. She begins with the tale of Aladdin and his wonderful lamp. For ages eight and older."
         },
         {
            id: 3,
            name: 'MY CHRISTMAS LOVE',
            pages:"Wafa’ Tarnowska ",
            published:"Barefoot Books (December 2010)",
            author:" Garrett Frawley, Brian Turner",
            image:require("./image/book4.jpg"),
            description:"A woman receives presents from an anonymous suitor who's inspired by the 12 Days of Christmas"
         },
         {
          id: 4,
          name: 'FAIRY TALES LITTLE CHILDERN',
            pages:"Wafa’ Tarnowska ",
            published:"Barefoot Books (December 2010)",
            author:" Garrett Frawley, Brian Turner",
          image:require("./image/book5.jpg"),
          description:"n this book, author and translator Wafa’ Tarnowska retells the stories of The Thousand and One Nights, or The Arabian Nights in a large, generously illustrated volume that is meant to be a permanent part of a home or library collection. And rightly so, because this ancient collection of tales and fables from Persia, India, and the Arab world has, as Tarnowska writes in her foreword, “become a part of the world’s cultural heritage.” In them, the bride Shahrazade saves her own life, and that of countless other young women, by telling tales for one thousand and one nights to her new husband, the king Shahriyar. She begins with the tale of Aladdin and his wonderful lamp. For ages eight and older."
        },
       {
        id: 5,
        name: 'BOYS ON SAFARI',
        pages:"Wafa’ Tarnowska ",
            published:"Barefoot Books (December 2010)",
            author:" Garrett Frawley, Brian Turner",
        image:require("./image/book6.jpg"),
        escription:"My Very Own® Name is made especially for each child, featuring the child’s first name, last name and birth date. In this beautifully illustrated story, animal characters bring letters one by one to spell the first and last names of the child.The tiger brings the T, the yak brings the Y, and so on, until the child’s first and last names are spelled in rhyme. At the end, the animals celebrate because they’ve created the perfect name. The book also includes an illustrated encyclopedia of 61 animals, with fun facts about each."
 },
     {
      id: 6,
      name: 'ALICE...FIRST DAY AT SCHOOL',
      pages:"20 pages ",
            published:"Published January 6th 2015 by bedtime stories for kids",
            author:" by Amelia K.",
      image:require("./image/book7.jpg"),
      description:"Alice is 3 years old girl, a delightful little girl who loves adventures and playing with her friends and her family. She is in learning age, everything is quite new for her. Alice invite you the see her life, her family, her activities and also her friends. It's a happy place, happy activity for a happy little girl and her friends. "
   },
   {
    id: 7,
    name: 'MY VERY OWN NAME',
            pages:"Wafa’ Tarnowska ",
            published:"Barefoot Books (December 2010)",
            author:" Garrett Frawley, Brian Turner",
    image:require("./image/book8.jpg"),
    description:"My Very Own® Name is made especially for each child, featuring the child’s first name, last name and birth date. In this beautifully illustrated story, animal characters bring letters one by one to spell the first and last names of the child.The tiger brings the T, the yak brings the Y, and so on, until the child’s first and last names are spelled in rhyme. At the end, the animals celebrate because they’ve created the perfect name. The book also includes an illustrated encyclopedia of 61 animals, with fun facts about each."
 },
 {
  id: 8,
  name: 'IF MY DOG COULD TALK ',
            pages:"Approximately 20 page",
            published:"Barefoot Books (December 2010)",
            author:" Garrett Frawley, Brian Turner",
  image:require("./image/book9.jpg"),
  description:"Ever wonder what your pet dog is thinking when he gives you that look?” Hilariously narrated from the dog’s perspective, this rhyming storybook celebrates the special, loving bond between dogs and their families."
},
{
  id: 9,
  name: 'WONDERS OF WONDERS ',
  pages:"Approximately 20 page",
  published:"Hardcover – October 22, 2013",
  author:"Alisa Solomon",
  image:require("./image/book10.jpg"),
  description:"In the half-century since its premiere, Fiddler on the Roof has had an astonishing global impact. Beloved by audiences the world over, performed from rural high schools to grand state theaters, Fiddler is a supremely potent cultural landmark. In a history as captivating as its subject, award-winning drama critic Alisa Solomon traces how and why the story of Tevye the milkman, the creation of the great Yiddish writer Sholem-Aleichem, was reborn as blockbuster entertainment and a cultural touchstone, not only for Jews and not only in America. It is a story of the theater, following Tevye from his humble appearance on the New York Yiddish stage, through his adoption by leftist dramatists as a symbol of oppression, to his Broadway debut in one of the last big book musicals, and his ultimate destination―a major Hollywood picture."
},
{
  id:10 ,
            name: 'SEA OF LOVE(Simon Romantic Comedies)',
            pages:"Paperback, 236 pages",
            published:"Published December 16th 2008 by Simon Pulse",
            author:"by Simon Pulse",
            image:require("./image/book1.jpg"),
            description:"Sea of Love is a 1989 American neo-noir thriller film directed by Harold Becker, written by Richard Price and starring Al Pacino, Ellen Barkin and John Goodman. The story concerns a New York City detective trying to catch a serial killer who finds victims through the singles column in a newspaper."
          },
{
  id: 11,
  name: 'THE ARABIAN NIGHTS',
            pages:"Wafa’ Tarnowska ",
            published:"Barefoot Books (December 2010)",
            author:"Carole Hénaff (Illustrator)",
            image:require("./image/book3.jpg"),
            description:"n this book, author and translator Wafa’ Tarnowska retells the stories of The Thousand and One Nights, or The Arabian Nights in a large, generously illustrated volume that is meant to be a permanent part of a home or library collection. And rightly so, because this ancient collection of tales and fables from Persia, India, and the Arab world has, as Tarnowska writes in her foreword, “become a part of the world’s cultural heritage.” In them, the bride Shahrazade saves her own life, and that of countless other young women, by telling tales for one thousand and one nights to her new husband, the king Shahriyar. She begins with the tale of Aladdin and his wonderful lamp. For ages eight and older."
         },
      ]
   }
   OpenSecondActivity (rowData)
   {
     
      this.props.navigation.navigate('Second', { ListViewClickItemHolder: rowData });
     
      
   }

   render() {
      return (
         <View> 
           <ScrollView>
            {
               this.state.names.map((item, index) => (
                
                 
                  
                  <TouchableOpacity
                     key = {item.id}
                     style = {styles.container}
                     onPress = {() => this.OpenSecondActivity(item)}>
                     
                     
                     <Image source={item.image} style={styles.image}/>
                     <Text style = {styles.text}>
                        {item.name}
                     </Text>
                  </TouchableOpacity>
                  
                 
                 
                  
               ))
            }
            </ScrollView>
         </View>
      )
   }
}
class SecondActivity extends Component
{
  static navigationOptions =
  {
     title: 'DETAILS',
  };
 
  render()
  {
     return(
      <ScrollView>
        <View style = { styles.MainContainer }>
        
          <Image source={this.props.navigation.state.params.ListViewClickItemHolder.image} style={styles.image}/>
          <Text style = {styles.headings}>DETAILS</Text>
           {/* <Text   style = {styles.detailText}>  { this.props.navigation.state.params.ListViewClickItemHolder.id }</Text> */}
          <Text  style = {styles.detailText}>{ this.props.navigation.state.params.ListViewClickItemHolder.name }</Text>
          <Text  style = {styles.detailText}>{ this.props.navigation.state.params.ListViewClickItemHolder.author }</Text>
          <Text  style = {styles.detailText}>{ this.props.navigation.state.params.ListViewClickItemHolder.published }</Text>
          <Text  style = {styles.detailText}>{ this.props.navigation.state.params.ListViewClickItemHolder.pages }</Text>
          <Text style = {styles.headings}>DESCRIPTION:-</Text>
          <Text  style = {styles.detailText}>{ this.props.navigation.state.params.ListViewClickItemHolder.description }</Text>
        
        </View>
        </ScrollView>

     );
  }
}
 
export default contactList = StackNavigator(
  {
    First: { screen:List},
    
    Second: { screen: SecondActivity }
  });

const styles = StyleSheet.create ({
   container: {
      padding: 10,
      marginTop: 3,
      backgroundColor: '#D3D3D3',
      flex:1, flexDirection: 'row'
      
   },
   text: {
    textAlignVertical:'center',
    width:'50%', 
    padding:20,
    fontWeight: 'bold',
     
   },
   image:{
    width: '50%',
    height: 200 ,
    margin: 10,
    borderRadius : 10
   },
   headings:{
    backgroundColor: 'white',
    fontWeight: 'bold',
    margin: 10,
    fontSize:20
   },
   detailText:{
     fontSize:15
   }
})
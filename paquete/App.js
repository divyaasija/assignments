import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
 import Register from './Register';
import Login from './Login';
import Dashboard from './Dashboard';
 import Location from './Location';
import ShopList from './ShopList';
import ShopListDetails from './ShopListDetails';
import Gallery from './Gallery';
import FriendList from './FriendList';
import { Router, Scene,ActionConst } from 'react-native-router-flux';


export default class App extends React.Component {
  render() {
    return (
      <Router>
      <Scene key="root">
       <Scene
          key="login"
          component={Login}
          title="LOGIN" 
          initial
        />
        <Scene 
          key="register"
          component={Register}
          title="REGISTER" 
        />
        <Scene
          key="Dashboard"
          component={Dashboard}
          title="DASHBOARD"
          type={ActionConst.RESET} 
        />
         <Scene
          key="Location"
          component={Location}
          title="Location"
          
        />
        <Scene
          key="ShopList"
          component={ShopList}
          title="Shop List"
        />
        <Scene
          key="Gallery"
          component={Gallery}
          title="Gallery" 
          panHandlers={null}
          
        />
        <Scene
          key="FriendList"
          component={FriendList}
          title="FriendList" 
        />
      </Scene>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

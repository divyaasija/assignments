import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CameraView from './Camera';
import CameraRoll from './CameraRoll';
import Welcome from './Welcome'
import { Router, Scene,ActionConst } from 'react-native-router-flux';


export default class App extends React.Component {
  render() {
    return (
      <Router>
      <Scene key="root">
       <Scene
          key="camera"
          component={CameraView}
          title="Camera"  
          onRight={ ()=> Actions.CameraRoll() }
          rightButtonImage={require('./images/galleryIcon.png')}
        />
        
        <Scene 
          key="CameraRoll"
          component={CameraRoll}
          title="Gallery"
          onRight={ ()=> Actions.camera() }
          rightButtonImage={require('./images/cameraIcon.png')}
        />
        
        <Scene 
          key="Welcome"
          component={Welcome}
          title="Welcome"
          initial
        />
        
      </Scene>
      </Router>
    );
  }
}


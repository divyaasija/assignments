import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux'; 

export default class Register extends React.Component {
  constructor(){
    super();
    this.state={
      registerDetails:{
        uname:'',
        password:'' ,
        fname:'',
        lname:''
      }
  }
  }
  
  storeRegistrationDetails=async ()=>{
    const username=this.state.registerDetails.uname
    const password=this.state.registerDetails.password
    const firstname=this.state.registerDetails.fname
    const lastname=this.state.registerDetails.lname
    AsyncStorage.setItem(
      username,
      JSON.stringify(this.state.registerDetails)
    );
     alert("Registered Successfully")
      Actions.login();
    }
  

  render() {
    return (
      <View style={styles.container}>
      <Image style={styles.logo}
          source={require('./images/logo.png')}
        />
     
      <View style={styles.inputBox}>
      <TextInput  placeholder="First Name" 
         onChangeText={fname =>
          this.setState({
            registerDetails: {
              ...this.state.registerDetails,
              fname: fname
            }
          })}
         />
         <TextInput  placeholder="Last Name" 
         onChangeText={lname =>
          this.setState({
            registerDetails: {
              ...this.state.registerDetails,
              lname: lname
            }
          })}
         />
      
      <TextInput  placeholder="User Name" 
         onChangeText={uname =>
          this.setState({
            registerDetails: {
              ...this.state.registerDetails,
              uname: uname
            }
          })}
         />
        <TextInput 
        secureTextEntry={true}
         placeholder="password" 
         onChangeText={password =>
          this.setState({
            registerDetails: {
              ...this.state.registerDetails,
              password: password
            }
          })
        }/>
      <Button
          style={styles.button}
          onPress={()=>this.storeRegistrationDetails()}
          title="Register"
          />
      </View>
      
    </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    height:155,
    width:150,
},


  inputBox:{
      width:300,
      marginVertical:25, 
  },
  button:{
    color:"#3ADF00"
  }
 
});

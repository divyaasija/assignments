import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,Button,Image,ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux'; 
//import GridView from 'react-native-super-grid';

export default class Dashboard extends React.Component {
  ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: .5,
          width: "100%",
          backgroundColor: "#000",
        }}
      />
    );
  }
    
render(){
    return(
      <View>
       
        {/* <GridView 
  itemWidth={130}
 
  items={["location","friend list","gallery","shop"]}
  renderItem={item => (<TouchableHighlight ><Text style={styles.grid}>{item}</Text></TouchableHighlight>)}
/> */}
<ScrollView>
<TouchableOpacity  onPress={() => Actions.Gallery()}>
  <Image style={styles.logo}
            source={require('./images/gallery.jpg')}
            />
            
</TouchableOpacity>
<TouchableOpacity  onPress={() => Actions.FriendList()}>
  <Image style={styles.logo}
            source={require('./images/list.png')}
            />
</TouchableOpacity>
<TouchableOpacity  onPress={() => Actions.Location()}>
  <Image style={styles.logo}
            source={require('./images/location.png')}
            />
</TouchableOpacity>
<TouchableOpacity  onPress={() => Actions.ShopList()}>
  <Image style={styles.logo}
            source={require('./images/shop.png')}
            />
</TouchableOpacity>
</ScrollView>
</View>

    )
}
}
const styles = StyleSheet.create({

  logo:{
    marginTop:20,
    height:165,
    width:'100%',
},
});


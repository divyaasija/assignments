import React from 'react';
import { View, TouchableHighlight, Image, StyleSheet, Dimensions, Text } from 'react-native'
import CameraRollPicker from 'react-native-camera-roll-picker';
import { Actions } from 'react-native-router-flux'
import { StackNavigator } from 'react-navigation';
export default class CameraRoll extends React.Component {
    state = {
        number: 0
    }
    selectedImage(images) {
        this.setState = ({
            number: images.length
        })
        alert("Number of images selected  " + images.length)
    }
    render() {
        return (
            <View >
                {/* <Text style={styles.selectedImageText}>{this.state.number} Images Selected</Text> */}
                <View style={styles.image}>
                    <Text style={styles.selectedImageText}>{this.state.number} Images Selected</Text>
                    <CameraRollPicker callback={this.selectedImage.bind(this)} />
                </View>
            </View>

        )
    }
}

// class image extends React.Component
// {

//   render()
//   {
//      return(

//         <View style = { styles.MainContainer }>

//           <Image source={{uri:this.props.navigation.navigate.cameraImage}} style={styles.image}/>

//         </View>


//      );
//   }
// }

// export default CameraRoll = StackNavigator(
//   {
//     First: { screen:gallery},

//     Second: { screen: image }
//   });

const styles = StyleSheet.create({
    image: {

        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    selectedImageText: {
        fontWeight: 'bold',
        textAlign: 'center'
    }

})
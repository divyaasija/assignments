import React from 'react';
import { StyleSheet, Text, View,ScrollView,Image,DrawerLayoutAndroid,TouchableHighlight,TouchableOpacity,TouchableNativeFeedback,TouchableWithoutFeedback,FlatList } from 'react-native';


export default class App extends React.Component {
  Event=()=>{
    alert("Image is triggered");
  }
  
  
  render() {
    var navigationView = (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>side Drawer view</Text>
      </View>
    );
    return (
    
      <View>
        <Text>hey</Text>
          <DrawerLayoutAndroid
      drawerWidth={100}
      drawerPosition={DrawerLayoutAndroid.positions.Left}
      renderNavigationView={() => navigationView}>
      <Text style={{margin: 10, fontSize: 15, textAlign: 'center'}}>SLIDE TO RIGHT</Text>
     
    </DrawerLayoutAndroid>
      <ScrollView>
        
      
        <TouchableHighlight onPress={()=>this.Event() }>
        <Image style={styles.container} source={require('./download.jpeg')}/>
        </TouchableHighlight>

        <TouchableOpacity onPress={()=>this.Event()}>
        <Image style={styles.container} source={require('./download.jpeg')}/> 
        </TouchableOpacity>

        <TouchableNativeFeedback onPress={()=> alert("hey")}>
        <Image style={styles.container} source={require('./download.jpeg')}/> 
        </TouchableNativeFeedback> 

         <TouchableWithoutFeedback onPress={()=> alert("hey")}>
        <Image style={styles.container} source={require('./download.jpeg')}/> 
        </TouchableWithoutFeedback> 

          <View style={styles.container}> 
         
        <FlatList
          data={[
            {key: 'ITEM 1'},
            {key: 'ITEM 2'},
            {key: 'ITEM 3'},
            {key: 'ITEM 4'},
          ]}
          renderItem={({item}) => 
          <Text style={styles.item}>{item.key}</Text>
          }}
        />
      </View>
        
     </ScrollView> 
        </View>
     );
        )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

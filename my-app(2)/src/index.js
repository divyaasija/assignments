import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

//creating custom element
const Greet=React.createElement('h1', {}, "this is React custom element") ;

//rendering custom element
ReactDOM.render(Greet, document.getElementById('child2'));

//jsx 
const Greeting =<h1>This is another element having JSX.</h1>
//rendering custom element
ReactDOM.render(Greeting, document.getElementById('child1'));
ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();

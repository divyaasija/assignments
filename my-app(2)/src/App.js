import React, { Component } from 'react';
import logo from './logo.svg';
import 'whatwg-fetch';
import './App.css';
//defining React component
const Intro=(
  <p>this is react element without using props</p>
)
function Login(props) {
  return <h1>Welcome back!</h1>;
}
const element = (
  <h1 >
    Hello, world!
  </h1>
);

function Logout(props) {
  return <h1>Please sign up.</h1>;
}
function Greeting(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <Login />;
  }
  return <Logout />;
}


const Intro1=(props)=>(
  <p>{props.message}</p>
)
class App extends Component {
  state={
    list:[],
    list1:[]
    
  }
  componentDidMount() {
    const list=["apple","banana","orange"];
    setTimeout(()=>{
      this.setState({list:list})
    },3000)
  }

  componentWillMount()
  {
    fetch('http://api.tvmaze.com/search/shows?q=%27vikings')
    .then((Response)=>Response.json())
    .then(json =>this.setState({list1:json}))
  }
  handel_onclick(event){
    event.preventDefault();
    alert("event handling in REACT")
  }
  
  render() {
    
    
    
    return (
     
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
          
        </header>
       <h1> 
        <greeting/></h1>
        
        <Intro />
        <Intro1 message="react element with props"/>
        size is= {this.state.list.length};<br/>
        using expression = {1+1}
        <br/>
        <button type="button" onClick={this.handel_onclick}>event_handling</button>
        <Greeting isLoggedIn={false} />
        size is= {this.state.list1.length};<br/>
        <element/>
      </div>
    );
  }
}

export default App;

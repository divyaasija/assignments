import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter, Route} from 'react-router-dom';
import abc from './abc.jpg';


const home=()=>{
    return(<h1>abcdef</h1>)
    
}
const image=()=>{
    return(<img className="imageDisplay" src={abc} alt={"image"}/>)
    
}
class forms extends Component{
    constructor(props){
        super(props);
        this.state={  
         inputText:"enter value"
          
        }
      }
    changeEvent=(event)=>{
        this.setState({inputText: event.target.value});
        
    }
    submitEvent=()=>{
        alert('Entered value is: ' + this.state.inputText);
    }
    render(){
    return(
        <form onSubmit={this.submitEvent} className="container">
            <input type="text" placeholder="enter value" onChange={this.changeEvent} className="Input-data"/>
            <button type="submit" className="Button-design">DISPLAY</button>
        </form>
    )
}
    
}
ReactDOM.render(<BrowserRouter>
<div>
<Route path='/'component={App}/>
<Route path='/home' component={home}/>
<Route path='/image' component={image}/>
<Route path='/form' component={forms}/>
</div>
</BrowserRouter>,
 document.getElementById('root'));
registerServiceWorker();

import React, { Component } from 'react';
import './App.css';
import {Link,NavLink} from 'react-router-dom'
import './xyz.jpg'
const hello=()=>{
 
}

const Introduction=()=>{
  return(
  <p className="App-intro">
  IMPLEMENTING REACT FEATURES
  </p>
  )
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
         
          <h1 className="App-title">Welcome to React </h1>
        </header>
        <Introduction/>
          <div className="App-Link">
          
          <NavLink to='/home' activeClassName="active">HOME   &nbsp;&nbsp;&nbsp;&nbsp;</NavLink>
          <NavLink to='/image' activeClassName="active" >IMAGE &nbsp;&nbsp;&nbsp;&nbsp;</NavLink>
          <Link to={{pathname:'/form'}} activeClassName="active">FORM</Link>
          </div>
          <h1>Welcome here!!</h1>
      </div>
    );
  }
}

export default App;

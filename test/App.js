import React, {Component} from 'react';
import { StyleSheet, Text,Image, View ,FlatList,TouchableHighlight,TouchableOpacity} from 'react-native';
import { StackNavigator } from 'react-navigation';
class App extends React.Component {
  constructor(props){
    super(props);
    this.state={list:[{key: 'A'},
    {key: 'B'},
    {key: 'C'},
    {key: 'D'},   
  ]
}}
  static navigationOptions =
  {
     title: 'CONTACT LIST',
  };
  eventTest=(itemName)=>{
    this.props.navigation.navigate(itemName);
  }
 
  
  
  render() {
    return (
      <View style={styles.container}>
          <FlatList
          data={this.state.list}
          renderItem={({item}) => <TouchableHighlight onPress={()=>this.eventTest(item.key)}>
          <Text  style={styles.list}>{item.key}</Text>
          </TouchableHighlight>}
        />
           
        </View>
    );
  }
}





class A extends Component
{
  static navigationOptions =
  {
     title: 'A',
  };
 
  render()
  {
     return(
        <View >
          <Image  source={require('./one.jpg')}/> 
           <Text > First name =  A </Text>
           <Text > Last name =  A </Text>
           <Text > Phone number =  99999 </Text>
 
        </View>
     );
  }
}
class B extends Component
{
  static navigationOptions =
  {
     title: 'B',
  };
 
  render()
  {
     return(
        <View >
          <Image  source={require('./two.png')}/> 
          <Text > First name =  B </Text>
           <Text > Last name =  B </Text>
           <Text > Phone number = 00000</Text>
          
 
        </View>
     );
  }
}
class C extends Component
{
  static navigationOptions =
  {
     title: 'C',
  };
 
  render()
  {
     return(
        <View >
          <Image  source={require('./three.png')} style={styles.image}/> 
          <View style={borderColor="black"}>
          <Text > First name =  C </Text>
           <Text > Last name =  C </Text>
           <Text > Phone number = 1111</Text>
          
           </View>
 
        </View>
     );
  }
}
class D extends Component
{
  static navigationOptions =
  {
     title: 'D',
  };
 
  render()
  {
     return(
        <View >
          <Image  source={require('./four.jpg')}/> 
          <Text > First name =  D </Text>
           <Text > Last name =  D </Text>
           <Text > Phone number = 88888</Text>
 
        </View>
     );
  }
}

export default Project = StackNavigator(
  {
   First: { screen: App },
   A: { screen: A },
   B: { screen: B  },
   C: { screen: C},
   D: { screen:D },
  
  });
  


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  list: {
    margin:40,
    height:40,
    backgroundColor:"grey",
    alignItems: 'center',
    width:50,
    borderColor:"black",
    fontWeight: 'bold'
  },
  image:{

  }
});
